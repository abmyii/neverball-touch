#!/bin/bash

set -Eeou pipefail

HOST_ARCH=x86_64
HOST_ARCH_TRIPLET=x86_64-linux-gnu

apt-get update
apt-get install libsdl2-dev:amd64 libpulse-dev:amd64 libglib2.0-dev:amd64 libegl1-mesa-dev:amd64 libgles2-mesa-dev:amd64 libmirclient-dev:amd64 libxkbcommon-dev:amd64 libjpeg62-dev:amd64 libphysfs-dev:amd64 libpng12-0:amd64 -y

cp /usr/include/$HOST_ARCH_TRIPLET/jconfig.h .
cp /usr/lib/$HOST_ARCH_TRIPLET/libpng12.so.0 libpng12.so

export C_INCLUDE_PATH="$PWD"
export CPLUS_INCLUDE_PATH="$PWD"
export LIBRARY_PATH="$PWD"

ARCH=$HOST_ARCH ARCH_TRIPLET=$HOST_ARCH_TRIPLET CC=gcc CXX=g++ make -j4 sols

make clean-src
