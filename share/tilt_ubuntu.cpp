/*
 * Copyright (C) 2003 Robert Kooima
 *
 * NEVERBALL is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General  Public License as published
 * by the Free  Software Foundation; either version 2  of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
 * MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
 * General Public License for more details.
 */

#include <SDL.h>
#include <QCoreApplication>
#include <QSensor>

#include "log.h"

static QCoreApplication* fakeQApp = nullptr;
static QSensor* sensor = nullptr;
static float accelx = 0;
static float accely = 0;
static float accelz = 0;
static int status = 0;

static void GetTiltData(QCoreApplication& app, QSensor& sensor) {
    app.processEvents(QEventLoop::AllEvents, 1);
    QSensorReading *reading = sensor.reading();

    if (!reading)
        return;

    accelx = reading->property("x").value<float>();
    accely = reading->property("y").value<float>();
    accelz = reading->property("z").value<float>();
}

extern "C" {
void tilt_init(void) {
    SDL_setenv("UBUNTU_PLATFORM_API_BACKEND", "touch_mirclient", 1);

    int fakeArgc = 0;
    char** fakeArgv = nullptr;
    fakeQApp = new QCoreApplication(fakeArgc, fakeArgv);
    sensor = new QSensor("QAccelerometer");
    sensor->start();
    status = 1;
}

void tilt_free(void) {
    status = 0;
}

int tilt_stat(void) {
    GetTiltData(*fakeQApp, *sensor);
    return status;
}

int tilt_get_button(int *b, int *s) {
    return 0;
}

float tilt_get_x(void) {
    return accelx;
}

float tilt_get_y(void) {
    return accelz;
}

float tilt_get_z(void) {
    return accely;
}
}
